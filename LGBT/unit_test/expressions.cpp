//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 5, 2019
//
//======================================================================================================================
#include <tut/tut.hpp>
//---------------------------------------------------------------------------------------------------------------------
#include "LogicGates.h"
#include "DemoBB.h"
//---------------------------------------------------------------------------------------------------------------------
struct exp_test_data {
    DemoBB bb;
    LogicGate::NodeState ns;

    Alternate ofd { "Open Front Door" };
    Alternate efd { "Enter Front Door" };

    Alternate obd { "Open Back Door" };
    Alternate ebd { "Enter Back Door" };
};
//=====================================================================================================================
namespace tut {
    using t_group = test_group<exp_test_data>;
    t_group expressions { "Behavior Tree Expressions Tests" };

    using expression_tests = t_group::object;
//=====================================================================================================================
// Expressions
//
    template <>
    template <>
    void expression_tests::test<1>() {
        std::string test_name { "Expressions Test - Not And" };
        set_test_name(test_name);

        {
            auto test_node = ( !(True * True));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("Not And Failed Failure"), ns, LogicGate::False);
        }
        {
            auto test_node = ( !(True * False));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("Not And Failed Success"), ns, LogicGate::True);
        }
        {
            auto test_node = ( !(False * True));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("Not And Failed Success"), ns, LogicGate::True);
        }
        {
            auto test_node = ( !(False * False));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("Not And Failed Success"), ns, LogicGate::True);
        }
    }
//---------------------------------------------------------------------------------------------------------------------
//
    template <>
    template <>
    void expression_tests::test<2>() {
        std::string test_name { "Expressions Test - Not Or" };
        set_test_name(test_name);

        {
            auto test_node = ( !(True + True));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("Not Or Failed Failure"), ns, LogicGate::False);
        }
        {
            auto test_node = ( !(True + False));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("Not Or Failed Failure"), ns, LogicGate::False);
        }
        {
            auto test_node = ( !(False + True));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("Not Or Failed Failure"), ns, LogicGate::False);
        }
        {
            auto test_node = ( !(False + False));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("Not Or Failed Success"), ns, LogicGate::True);
        }
    }
    //---------------------------------------------------------------------------------------------------------------------
    //
    template <>
    template <>
    void expression_tests::test<3>() {
        std::string test_name { "Expressions Test - And Or" };
        set_test_name(test_name);

        {
            auto test_node = ((True * True) + (True * True));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("And Or TT+TT Failed Success"), ns, LogicGate::True);
        }
        {
            auto test_node = ((False * False) + (True * True));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("And Or FF+TT Failed Success"), ns, LogicGate::True);
        }
        {
            auto test_node = ((True * True) + (False * False));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("And Or TT+FF Failed Success"), ns, LogicGate::True);
        }
        {
            auto test_node = ((False * False) + (False * False));
            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("And Or FF+FF Failed Failure"), ns, LogicGate::False);
        }
    }
    //---------------------------------------------------------------------------------------------------------------------
    //
    template <>
    template <>
    void expression_tests::test<11>() {
        std::string test_name { "Expressions Test - Root" };
        set_test_name(test_name);

        {
            auto tree = (True * True) + (True * True);
            Subtree test_node { "root", tree };

            ns = test_node.tick(bb);
            ensure_equals(test_name + std::string("Root TT+TT Failed Success"), ns, LogicGate::True);
        }
    }
    //---------------------------------------------------------------------------------------------------------------------
    //
    template <>
    template <>
    void expression_tests::test<20>() {
        std::string test_name { "Print Test - Root" };
        set_test_name(test_name);
//        GateTraceOn g_on(20);

        auto tree { ((ofd * efd) + (obd * ebd)) };
        while (tree.tick(bb) != LogicGate::True) {
            std::cerr << '\n';
        }
    }
    //---------------------------------------------------------------------------------------------------------------------
    //
//    template <>
//    template <>
//    void expression_tests::test<21>() {
//        std::string test_name { "Print Test - Root" };
//        set_test_name(test_name);
////        GateTraceOn g_on(21);
//
//        auto try_front { (ofd * efd) };
////        auto try_front = { "try_front", (ofd * efd) };
//        std::cerr << '\n';
//        auto try_back { (obd * ebd) };
//        std::cerr << '\n';
//
//        int depth { };
////        (try_front + try_back).print(std::cout, depth);
////        std::cerr << "while \n\n";
//        GateTraceOn g_on(21);
//
//        while ((try_front + try_back).tick(bb) != LogicGate::True) {
//            std::cerr << "while \n\n";
//            (try_front + try_back).print(std::cout, depth);
//
//            std::cerr << '\n';
//        }
//        std::cerr << "done \n\n";
//    }
//---------------------------------------------------------------------------------------------------------------------
//
    template <>
    template <>
    void expression_tests::test<30>() {
        std::string test_name { "Print Test - Root" };
        set_test_name(test_name);
//        GateTraceOn g_on(30);

        auto tree { ((ofd * efd) + !(obd + ebd)) * (True + False * Running) };
        Subtree root { "root", tree };

//        int depth { };
//        root.print(std::cout, depth);
        while (root.tick(bb) != LogicGate::True) {
            std::cerr << '\n';
        }
    }
}
// namespace end

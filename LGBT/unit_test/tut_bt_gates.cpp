//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 5, 2019
//      Based on material form TUT
//
//======================================================================================================================
#include <tut/tut.hpp>
#include <tut/tut_reporter.hpp>

#include <unistd.h>
#include "Logfault.h"

#include "GateTrace.h"
int8_t GateTrace::mDepth { 0 };

#include <tut/tut.hpp>
#include <tut/tut_reporter.hpp>

namespace tut {
    test_runner_singleton runner;
}

int LogicGate::mCnt { };

int main() {

    tut::reporter reporter;
    tut::runner.get().set_callback( &reporter);

    tut::test_result tr;

    GateTrace::off();

    tut::runner.get().run_tests("Logic Not & Alternate Gate Behavior Tree Tests");
    tut::runner.get().run_tests("Logic Or Gate Behavior Tree Tests");
    tut::runner.get().run_tests("Logic And Gate Behavior Tree Tests");
    tut::runner.get().run_tests("Behavior Tree Expressions Tests");

    return !reporter.all_ok();
}


//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 5, 2019
//
//======================================================================================================================
#include <tut/tut.hpp>
//---------------------------------------------------------------------------------------------------------------------
#include "LogicGates.h"
#include "DemoBB.h"
//---------------------------------------------------------------------------------------------------------------------
struct and_test_data {
    DemoBB bb;
    LogicGate::NodeState ns;
};
//=====================================================================================================================
namespace tut {

    using t_group = test_group<and_test_data>;
    t_group and_gate { "Logic And Gate Behavior Tree Tests" };

    using and_gate_tests = t_group::object;
    //=====================================================================================================================
    // AndGate
    //
    template <>
    template <>
    void and_gate_tests::test<1>() {
        // GateTraceOn g_on(1);
        set_test_name("And Gate Test - default params");

        AndGate test_node(True, True);

        ns = test_node.tick(bb);
        ensure_equals("AndGate failed default inputs for Success", ns, LogicGate::True);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void and_gate_tests::test<2>() {
        // GateTraceOn g_on(2);
        set_test_name("And Gate Test - True / True");

        AndGate test_node("AndGate", True, True);

        ns = test_node.tick(bb);
        ensure_equals("AndGate failed True / True inputs for Success", ns, LogicGate::True);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void and_gate_tests::test<3>() {
        // GateTraceOn g_on(3);
        set_test_name("And Gate Test - False / True");

        AndGate test_node("AndGate", False, True);

        ns = test_node.tick(bb);
        ensure_equals("AndGate failed False / True inputs for Failure", ns, LogicGate::False);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void and_gate_tests::test<4>() {
        // GateTraceOn g_on(4);
        set_test_name("And Gate Test - True / False");

        AndGate test_node("AndGate", True, False);

        ns = test_node.tick(bb);
        ensure_equals("AndGate failed True / False inputs for Failure", ns, LogicGate::False);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void and_gate_tests::test<5>() {
        // GateTraceOn g_on(5);
        set_test_name("And Gate Test - False / False");

        AndGate test_node("AndGate", False, False);

        ns = test_node.tick(bb);
        ensure_equals("AndGate failed False / False inputs for Failure", ns, LogicGate::False);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void and_gate_tests::test<6>() {
        // GateTraceOn g_on(6);
        set_test_name("And Gate Operator '*' - False / False");

        ns = (False * False).tick(bb);
        ensure_equals("AndGate Operator '*' failed False / False inputs for Failure", ns, LogicGate::False);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void and_gate_tests::test<7>() {
        // GateTraceOn g_on(7);
        set_test_name("And Gate Operator '*' - True / False");

        ns = (True * False).tick(bb);
        ensure_equals("AndGate Operator '*' failed True / False inputs for Failure", ns, LogicGate::False);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void and_gate_tests::test<8>() {
        // GateTraceOn g_on(8);
        set_test_name("And Gate Operator '*' - False / True");

        ns = (False * True).tick(bb);
        ensure_equals("AndGate Operator '*' failed False / True inputs for Failure", ns, LogicGate::False);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void and_gate_tests::test<9>() {
        // GateTraceOn g_on(9);
        set_test_name("And Gate Operator '*' - True / True");

        ns = (True * True).tick(bb);
        ensure_equals("AndGate Operator '*' failed True / True inputs for Success", ns, LogicGate::True);
    }
#if 0
//=====================================================================================================================
// AndQuadGate
//
template <>
template <>
void and_gate_tests::test<11>() {
    // GateTraceOn g_on(1); set_test_name("And Quad Gate Test - default params");

    AndQuadGate test_node("AndQuadGate");

    ns = test_node.tick(bb);
    ensure_equals("AndQuadGate failed default inputs for Success", ns, LogicGate::True);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void and_gate_tests::test<12>() {
    // GateTraceOn g_on(1); set_test_name("And Quad Gate Test - True / True / True / True ");

    AndQuadGate test_node("AndQuadGate", True, True, True, True);

    ns = test_node.tick(bb);
    ensure_equals("AndQuadGate failed True / True / True / True inputs for Success", ns, LogicGate::True);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void and_gate_tests::test<13>() {
    // GateTraceOn g_on(1); set_test_name("And Quad Gate Test - False / True / True / True ");

    AndQuadGate test_node("AndQuadGate", False, True, True, True);

    ns = test_node.tick(bb);
    ensure_equals("AndQuadGate failed False / True / True / True inputs for Failure", ns, LogicGate::False);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void and_gate_tests::test<14>() {
    // GateTraceOn g_on(1); set_test_name("And Quad Gate Test - True / False / True / True ");

    AndQuadGate test_node("AndQuadGate", True, False, True, True);

    ns = test_node.tick(bb);
    ensure_equals("AndQuadGate failed True / False / True / True inputs for Failure", ns, LogicGate::False);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void and_gate_tests::test<15>() {
    // GateTraceOn g_on(1); set_test_name("And Quad Gate Test - True / True / False / True ");

    AndQuadGate test_node("AndQuadGate", True, True, False, True);

    ns = test_node.tick(bb);
    ensure_equals("AndQuadGate failed True / True / False / True inputs for Failure", ns, LogicGate::False);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void and_gate_tests::test<16>() {
    // GateTraceOn g_on(1); set_test_name("And Quad Gate Test - True / True / True / False ");

    AndQuadGate test_node("AndQuadGate", True, True, True, False);

    ns = test_node.tick(bb);
    ensure_equals("AndQuadGate failed True / True / True / False inputs for Failure", ns, LogicGate::False);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void and_gate_tests::test<17>() {
    // GateTraceOn g_on(1); set_test_name("And Quad Gate Test - False / False / False / False ");

    AndQuadGate test_node("AndQuadGate", False, False, False, False);

    ns = test_node.tick(bb);
    ensure_equals("AndQuadGate failed False / False / False / False inputs for Failure", ns, LogicGate::False);
}
//=====================================================================================================================
// AndGate Complex
//
template <>
template <>
void and_gate_tests::test<18>() {
    // GateTraceOn g_on(1); set_test_name("And and Not Gate Test");
    AndGate test_node("AndGate", True, True);

    NotGate root {!(test_node)};

    ns = root.tick(bb);
    ensure_equals("AndGate failed default inputs for Failure", ns, LogicGate::True);

//         ns = ( !(True * True)).tick(bb);
//        ensure_equals("AndGate failed default inputs for Failure", ns, LogicGate::Failure);
}
#endif
}
// namespace end

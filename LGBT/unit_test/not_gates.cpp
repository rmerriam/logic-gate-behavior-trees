//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 5, 2019
//
//======================================================================================================================
#include <tut/tut.hpp>
//---------------------------------------------------------------------------------------------------------------------
#include "LogicGates.h"
#include "Alternate.h"
#include "DemoBB.h"
//---------------------------------------------------------------------------------------------------------------------
struct not_test_data {
    DemoBB bb;
    Alternate alt1 { "alt1" };
    LogicGate::NodeState ns;
};
//=====================================================================================================================
namespace tut {

    using t_group = test_group<not_test_data>;
    t_group not_gate { "Logic Not & Alternate Gate Behavior Tree Tests" };

    using not_gate_tests = t_group::object;
//=====================================================================================================================
// NotGate
//
    template <>
    template <>
    void not_gate_tests::test<1>() {
//        GateTraceOn g_on(1);
        set_test_name(" Not Gate Constructor -  No name both True & False");

        {
            NotGate test_node { ::True };
            ns = test_node.tick(bb);
            ensure_equals("NotGate failed True to Failure", ns, LogicGate::False);
        }
        {
            NotGate test_node(::False);

            ns = test_node.tick(bb);
            ensure_equals("NotGate failed False to Success", ns, LogicGate::True);
        }
    }
//---------------------------------------------------------------------------------------------------------------------
// NotGate
//
    template <>
    template <>
    void not_gate_tests::test<2>() {
//        GateTraceOn g_on(2);
        set_test_name("Not Gate Constructor -  Named both True & False");

        // uses template <typename T, typename = check_type<T>> NotGate(const std::string& name, T&& g) :
        {
            NotGate test_node { "Not True", True };
            ns = test_node.tick(bb);
            ensure_equals("NotGate failed True to Failure", ns, LogicGate::False);
        }
        {
            NotGate test_node("Not False", False);
            ns = test_node.tick(bb);
            ensure_equals("NotGate failed False to Success", ns, LogicGate::True);
        }
    }
//---------------------------------------------------------------------------------------------------------------------
//
    template <>
    template <>
    void not_gate_tests::test<3>() {
        // GateTraceOn g_on(3);
        set_test_name("Not Gate Constructor & Op -  True & False");

        // uses template <typename T, typename = check_type<T>> NotGate(const std::string& name, T&& g) and
        // Op! for double negative
        {
            NotGate test_node("NotGate", !True);
            ns = test_node.tick(bb);
            ensure_equals("NotGate failed False to Success", ns, LogicGate::True);
        }
        {
            NotGate test_node("NotGate", !False);
            ns = test_node.tick(bb);
            ensure_equals("NotGate failed True to Failure", ns, LogicGate::False);
        }
    }
//---------------------------------------------------------------------------------------------------------------------
// NotGate operator !
//
    template <>
    template <>
    void not_gate_tests::test<10>() {
        // GateTraceOn g_on(10);
        set_test_name("Not Gate -  True & False");

        // op! and uses explicit constructor
        ns = ( !True).tick(bb);
        ensure_equals("NotGate failed True to Failure", ns, LogicGate::False);

        ns = ( !False).tick(bb);
        ensure_equals("NotGate failed False to Success", ns, LogicGate::True);
    }
//=====================================================================================================================
// Alternate = test that the test routine Alternate works as intended
// Alternate starts with a state of failure. After tick it returns Success, then Running, then Failure
//
    template <>
    template <>
    void not_gate_tests::test<21>() {
        // GateTraceOn g_on(21);
        set_test_name("Alternate Node Test");

        LogicGate::NodeState ns { alt1.tick(bb) };
        ensure_equals("Alternate failed Running", ns, LogicGate::Run);

        ns = alt1.tick(bb);
        ensure_equals("Alternate failed Failure", ns, LogicGate::False);

        ns = alt1.tick(bb);
        ensure_equals("Alternate failed Success", ns, LogicGate::True);
    }
} // namespace end

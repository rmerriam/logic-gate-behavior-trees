//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 5, 2019
//
//======================================================================================================================
#include <tut/tut.hpp>
//---------------------------------------------------------------------------------------------------------------------
#include "LogicGates.h"
#include "DemoBB.h"
//---------------------------------------------------------------------------------------------------------------------
struct or_test_data {
    DemoBB bb;
    LogicGate::NodeState ns;
};
//=====================================================================================================================
namespace tut {

    using t_group = test_group<or_test_data>;
    t_group or_gate { "Logic Or Gate Behavior Tree Tests" };

    using or_gate_tests = t_group::object;
    //=====================================================================================================================
    // OrGate
    //
    template <>
    template <>
    void or_gate_tests::test<1>() {
        // GateTraceOn g_on(1);
        set_test_name("Or Gate Test - default params");

        OrGate test_node(True, True);

        ns = test_node.tick(bb);
        ensure_equals("OrGate failed default inputs for Failure", ns, LogicGate::True);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void or_gate_tests::test<2>() {
        // GateTraceOn g_on(2);
        set_test_name("Or Gate Test - True / True");

        OrGate test_node("OrGate", True, True);

        ns = test_node.tick(bb);
        ensure_equals("OrGate failed True / True inputs for Success", ns, LogicGate::True);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void or_gate_tests::test<3>() {
        // GateTraceOn g_on(3);
        set_test_name("Or Gate Test - False / True");

        OrGate test_node("OrGate", False, True);

        ns = test_node.tick(bb);
        ensure_equals("OrGate failed False / True inputs for Success", ns, LogicGate::True);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void or_gate_tests::test<4>() {
        // GateTraceOn g_on(4);
        set_test_name("Or Gate Test - True / False");

        OrGate test_node("OrGate", True, False);

        ns = test_node.tick(bb);
        ensure_equals("OrGate failed True / False inputs for Success", ns, LogicGate::True);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void or_gate_tests::test<5>() {
        // GateTraceOn g_on(5);
        set_test_name("Or Gate Test - False / False");

        OrGate test_node("OrGate", False, False);

        ns = test_node.tick(bb);
        ensure_equals("OrGate failed False / False inputs for Failure", ns, LogicGate::False);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void or_gate_tests::test<6>() {
        // GateTraceOn g_on(6);
        set_test_name("Or Gate Operator '+' - False / False");

        ns = (False + False).tick(bb);
        ensure_equals("OrGate Operator '+' failed False / False inputs for Failure", ns, LogicGate::False);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void or_gate_tests::test<7>() {
        // GateTraceOn g_on(7);
        set_test_name("Or Gate Operator '+' - True / False");

        ns = (True + False).tick(bb);
        ensure_equals("OrGate Operator '+' failed True / False inputs for Success", ns, LogicGate::True);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void or_gate_tests::test<8>() {
        // GateTraceOn g_on(8);
        set_test_name("Or Gate Operator '+' - False / True");

        ns = (False + True).tick(bb);
        ensure_equals("OrGate Operator '+' failed False / True inputs for Success", ns, LogicGate::True);
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <>
    template <>
    void or_gate_tests::test<9>() {
        // GateTraceOn g_on(9);
        set_test_name("Or Gate Operator '+' - True / True");

        ns = (True + True).tick(bb);
        ensure_equals("OrGate Operator '+' failed True / True inputs for Success", ns, LogicGate::True);
    }
#if 0
//=====================================================================================================================
// OrQuadGate
//
template <>
template <>
void or_gate_tests::test<11>() {
    // GateTraceOn g_on(1); set_test_name("Or Quad Gate Test - default params");

    OrQuadGate test_node("OrQuadGate");

    ns = test_node.tick(bb);
    ensure_equals("OrQuadGate failed default inputs for Success", ns, LogicGate::True);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void or_gate_tests::test<12>() {
    // GateTraceOn g_on(1); set_test_name("Or Quad Gate Test - True / True / True / True ");

    OrQuadGate test_node("OrQuadGate", True, True, True, True);

    ns = test_node.tick(bb);
    ensure_equals("OrQuadGate failed True / True / True / True inputs for Success", ns, LogicGate::True);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void or_gate_tests::test<13>() {
    // GateTraceOn g_on(1); set_test_name("Or Quad Gate Test - False / True / True / True ");

    OrQuadGate test_node("OrQuadGate", False, True, True, True);

    ns = test_node.tick(bb);
    ensure_equals("OrQuadGate failed False / True / True / True inputs for Success", ns, LogicGate::True);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void or_gate_tests::test<14>() {
    // GateTraceOn g_on(1); set_test_name("Or Quad Gate Test - True / False / True / True ");

    OrQuadGate test_node("OrQuadGate", True, False, True, True);

    ns = test_node.tick(bb);
    ensure_equals("OrQuadGate failed True / False / True / True inputs for Success", ns, LogicGate::True);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void or_gate_tests::test<15>() {
    // GateTraceOn g_on(1); set_test_name("Or Quad Gate Test - True / True / False / True ");

    OrQuadGate test_node("OrQuadGate", True, True, False, True);

    ns = test_node.tick(bb);
    ensure_equals("OrQuadGate failed True / True / False / True inputs for Success", ns, LogicGate::True);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void or_gate_tests::test<16>() {
    // GateTraceOn g_on(1); set_test_name("Or Quad Gate Test - True / True / True / False ");

    OrQuadGate test_node("OrQuadGate", True, True, True, False);

    ns = test_node.tick(bb);
    ensure_equals("OrQuadGate failed True / True / True / False inputs for Success", ns, LogicGate::True);
}
//---------------------------------------------------------------------------------------------------------------------
template <>
template <>
void or_gate_tests::test<17>() {
    // GateTraceOn g_on(1); set_test_name("Or Quad Gate Test - False / False / False / False ");

    OrQuadGate test_node("OrQuadGate", False, False, False, False);

    ns = test_node.tick(bb);
    ensure_equals("OrQuadGate failed False / False / False / False inputs for Failure", ns, LogicGate::False);
}
#endif
}
    // namespace end

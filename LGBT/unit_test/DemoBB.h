#ifndef DEMOBB_H_
#define DEMOBB_H_
//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 30, 2019
//
//======================================================================================================================
#include "BlackBoard.h"
#include "LogicGate.h"
//---------------------------------------------------------------------------------------------------------------------
class BlackBoard;
//---------------------------------------------------------------------------------------------------------------------
class DemoBB : public BlackBoard {
public:
    DemoBB() :
        BlackBoard() {
    }
//    static DemoBB& getDemoBB(BlackBoard& bb);

    std::string mData { "test output" };

};
//---------------------------------------------------------------------------------------------------------------------
//inline DemoBB& DemoBB::getDemoBB(BlackBoard& bb) {
//    return dynamic_cast<DemoBB&>(bb);
//
//}
#endif /* DEMOBB_H_ */


#ifndef ISDOORCLOSED_H_
#define ISDOORCLOSED_H_
//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 14, 2019
//
//======================================================================================================================

class IsDoorClosed : public Condition {
public:
    enum DoorLocation {
        front, back
    };
    IsDoorClosed(const std::string& name, DoorLocation loc) :
        Condition { name }, mDoorLocation { loc } {
        setState (True);
    }

    virtual LogicGate::NodeState tick(BlackBoard&) override {
        GateTrace depth { *this };

        switch (getState()) {

            case True:
                setState (Run);
                break;

            case Run:
                setState (False);
                break;

            case False:
                setState (True);
                break;
        }
        return getState();
    }

private:
    const DoorLocation mDoorLocation;
};

#endif /* ISDOORCLOSED_H_ */


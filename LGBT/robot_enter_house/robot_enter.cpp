//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 8, 2019
//
//======================================================================================================================
#include <unistd.h>
#include "Logfault.h"
#include "LogicGates.h"
#include "RobotEnterBB.h"

int8_t GateTrace::mDepth { };
int LogicGate::mCnt { };
//---------------------------------------------------------------------------------------------------------------------
#include "robot_enter.h"
//---------------------------------------------------------------------------------------------------------------------
int main() {
    logfault::LogManager::Instance().AddHandler(std::make_unique<logfault::StreamHandler>(std::cout, logfault::LogLevel::TRACE));
    std::cerr << std::endl;

    GateTrace::off();
//    GateTrace::on();

    RobotEnterBB robot_bb;
    StaticGate True { LogicGate::True };
    StaticGate False { LogicGate::False };
    StaticGate Running { LogicGate::Run };

    IsDoorClosed is_fdc { "is_front_door_closed", IsDoorClosed::front };
    OpenFrontDoor ofd;
    EnterFrontDoor efd;

    OpenBackDoor obd;
    EnterBackDoor ebd;

    LFLOG_DEBUG << "Logging to std::clog is enabled at DEBUG level";
    std::cerr << std::endl;
    ::sleep(1);
#if 0
// desn't work because temporary in while() is recreated every time
    auto try_front { (is_fdc + ofd) * efd };
    auto try_back { (obd * ebd) };
    std::cerr << '\n';

//    while ((try_front + try_back).tick(robot_bb) != LogicGate::True) {

    for (auto root { try_front + try_back }; root.tick(robot_bb) != LogicGate::True;) {
        std::cerr << '\n';
    }

#elif 0
    is_fdc.tick(robot_bb);
    is_fdc.tick(robot_bb);

    Subtree try_front { "try front", !is_fdc + ofd) * efd  };
    Subtree try_back { "try back", obd * ebd };
    Subtree try_enter_house { "try enter house", try_front + try_back };

    // print the tree
    int8_t depth { };
    try_enter_house.walk(std::cout, depth);
    std::cout << '\n';

    while (try_enter_house.tick(robot_bb) != LogicGate::True) {
        std::cerr << '\n';
    }

#elif 0

    auto tree1 { ( !is_fdc + ofd) * efd  };
    auto tree2 { ((obd * ebd)) };
    auto tree { tree1 + tree2 };
    Subtree root { "root", tree };
    std::cerr << std::endl;
    std::cout << std::endl;

    // print the tree
    int8_t depth { };
    root.walk(std::cout, depth);
    std::cout << '\n';

    while (root.tick(robot_bb) != LogicGate::True) {
        std::cerr << '\n';
    }

#elif 1

    Subtree try_front { "try_front", ( !is_fdc * ofd) * efd };
    Subtree try_back { "try_back", obd * ebd };
    Subtree try_enter_house { "try_enter_house", try_front + try_back };
    std::cerr << std::endl;
    std::cout << std::endl;

//    // print the tree
//    int8_t depth { };
//    try_enter_house.walk(std::cout, depth);
//    std::cerr << std::endl;
//    std::cout << std::endl;

    while (try_enter_house.tick(robot_bb) != LogicGate::True) {
        std::cerr << '\n';
    }

    std::cerr << std::endl;
    std::cout << std::endl;
#endif
    return 0;
}

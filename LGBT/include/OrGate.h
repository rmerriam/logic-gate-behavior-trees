#ifndef ORGATE_H_
#define ORGATE_H_
//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 5, 2019
//
//======================================================================================================================
class OrGate final : public TwoInputGate<LogicGate::OrGate> {
public:
    using TwoInputGate<LogicGate::OrGate>::TwoInputGate;

    virtual LogicGate::NodeState tick(BlackBoard& bb) override {
        GateTrace depth { *this };

        mG1->tick(bb);

        switch (mG1->getState()) {

            case True:
                setState (True);
                break;

            case Run:
                setState (Run);
                break;

            case False:
                setState(mG2->tick(bb));
                break;
        }
        return getState();
    }
};
//--------------------------------------------------------------------------------------------------------------------------
template <typename T1, typename T2>
inline constexpr return_check<T1, T2, OrGate> operator +(T1&& lhs, T2&& rhs) {
    return OrGate { "op+", make_concrete(lhs), make_concrete(rhs) };
}
#endif /* ORGATE_H_ */

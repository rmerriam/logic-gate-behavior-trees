#ifndef NOTGATE_H_
#define NOTGATE_H_
//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 5, 2019
//
//======================================================================================================================
class NotGate final : public Decorator<LogicGate::NotGate> {
public:
    using Decorator<LogicGate::NotGate>::Decorator;

    virtual LogicGate::NodeState tick(BlackBoard& bb) override {
        GateTrace depth { *this };

        mG->tick(bb);

        switch (mG->getState()) {

            case True:
                setState (False);
                break;

            case Run:
                setState (Run);
                break;

            case False:
                setState (True);
                break;
        }
        return getState();
    }

private:
};
//--------------------------------------------------------------------------------------------------------------------------
template <typename T>
inline constexpr return_check<T, T, NotGate> operator !(T&& rhs) {
//    std::cerr << "op! " << std::endl;
    return NotGate("op!", rhs);
}
#endif /* NOTGATE_H_ */

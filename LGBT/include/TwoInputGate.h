#ifndef TWOINPUTGATE_H_
#define TWOINPUTGATE_H_
//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 11, 2019
//
//======================================================================================================================
template <typename LogicGate::NodeType NT>
class TwoInputGate : public LogicGate {
public:
    explicit TwoInputGate(const std::string& name, LogicGatePtr g1, LogicGatePtr g2) :
        LogicGate { NT, name }, mG1 { std::move(g1) }, mG2 { std::move(g2) } {

//        std::cerr << "Explicit " << name << ' ' << lookupNodeText(NT) << '\n'   //
//            << mG1->getNodeType() << " [" << mG1->getName() << "] " << typeid( *mG1).name() << '\n'  //
//            << mG2->getNodeType() << " [" << mG2->getName() << "] " << typeid( *mG2).name();
//        std::cerr << '\n';
    }

    template <typename T1, typename T2>
    TwoInputGate(T1& g1, T2& g2) :
        TwoInputGate { lookupNodeText(NT), g1, g2 } {
//        std::cerr << "No Name " << lookupNodeText(NT) << '\n';

    }

    template <typename T1, typename = check_type<T1>, typename T2, typename = check_type<T2>>
    TwoInputGate(const std::string& name, T1&& g1, T2&& g2) :
        TwoInputGate { name, make_concrete(g1), make_concrete(g2) } {
//        std::cerr << "Named " << name << lookupNodeText(NT) << '\n';
    }

    TwoInputGate(TwoInputGate&& tig) :
        LogicGate { tig }, mG1 { std::move(tig.mG1) }, mG2 { std::move(tig.mG2) } {
        tig.mG1.reset();
        tig.mG2.reset();
    }
    ~TwoInputGate() {
        mG1.reset();
        mG2.reset();
    }
    virtual void walk(std::ostream& os, int8_t depth) const override {
        LogicGate::walk(os, depth);
        ++depth;
        mG1->walk(os, depth);
        mG2->walk(os, depth);
        --depth;
    }
protected:
    LogicGatePtr mG1;
    LogicGatePtr mG2;
};

#endif /* TWOINPUTGATE_H_ */


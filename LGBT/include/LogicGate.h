#ifndef LOGICGATE_H_
#define LOGICGATE_H_
//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 5, 2019
//
//======================================================================================================================
#include <BlackBoard.h>
class LogicGate;
using LogicGatePtr = std::unique_ptr<LogicGate >;
//--------------------------------------------------------------------------------------------------------------------------
class LogicGate {
public:
    enum NodeState {
        False, True, Run
    };

    enum NodeType {
        Action, Condition, Decorator, AndGate, OrGate, NotGate, Subtree
    };

    LogicGate(const NodeType nt, const std::string& name = "LogicGate") :
        mName { name }, mType { nt } {
        ++mCnt;
        mTag = mCnt;
        LFLOG_DEBUG << mTag << ' ' << getNodeType() << " X{" << getName() << "} " << this;
    }
//    LogicGate(const LogicGate& g) :
//        LogicGate { g.mType, g.mName } {
////        LFLOG_DEBUG << getNodeType() << " C{" << getName() << "} ";
//    }
//    LogicGate(LogicGate&& g) :
//        LogicGate { g.mType, g.mName } {
////        LFLOG_DEBUG << getNodeType() << " M{" << getName() << "} ";
//    }
//
    ~LogicGate() {
        LFLOG_DEBUG << mTag << ' ' << getNodeType() << " D{" << getName() << "} " << this;
    }
    virtual NodeState tick(BlackBoard& bb) = 0;

    const std::string& getName() const;
    const std::string getNodeId() const;
    const std::string getNodeType() const;

    LogicGate::NodeState getState() const;
    const std::string& getStateText(const LogicGate::NodeState ns) const;
    virtual void walk(std::ostream& os, int8_t depth) const;

protected:
    void setState(const NodeState new_state);
    const std::string lookupNodeText(NodeType nodeType) const;

private:

    const std::string mName;
    NodeState mState { False };
    NodeType mType;
    int mTag;
    static int mCnt;

};
//======================================================================================================================
//  utility usings
//
//--------------------------------------------------------------------------------------------------------------------------
template <typename T>
inline auto make_concrete(T&& rhs) {
    using NRT = typename std::remove_reference<T>::type;
    return std::make_unique < NRT > (std::move(rhs));
}
// check if T is derived from LogicGate. A reference is not a derived class so get the underlying type of T
template <typename T>
inline constexpr bool check_base = std::is_base_of_v<LogicGate, typename std::remove_reference_t<T>>;
//--------------------------------------------------------------------------------------------------------------------------
// These two expressions are used to prevent the compiler from using a function for types that are not valid
// check if the type of T is a derived class of LogicGate. Used in template declaration to remove
// improper attempts by compiler to use a function, i.e. apply op+ to string
template <typename T>
using check_type = std::enable_if_t<check_base<T>, T>;

template <typename T1, typename T2, typename G>
using return_check = std::enable_if_t<check_base<T1> && check_base<T2>, G>;
//======================================================================================================================
//  Start of inline implementaitions
//
inline LogicGate::NodeState LogicGate::getState() const {
    return mState;
}
//--------------------------------------------------------------------------------------------------------------------------
inline const std::string& LogicGate::getName() const {
    return mName;
}
//---------------------------------------------------------------------------------------------------------------------
inline const std::string& LogicGate::getStateText(const LogicGate::NodeState ns) const {
    static const std::string state_text[Run + 1] { "False", "True", "Run" };
    return state_text[ns];
}
//--------------------------------------------------------------------------------------------------------------------------
inline const std::string LogicGate::getNodeId() const {
    return typeid( *this).name();
    }
//--------------------------------------------------------------------------------------------------------------------------
    inline const std::string LogicGate::lookupNodeText(NodeType nodeType) const {
        const static std::string node_text[Subtree + 1] { "Action", "Condition", "Decorator", "AndGate", "OrGate", "NotGate", "Subtree" };
        return node_text[nodeType];
    }
//--------------------------------------------------------------------------------------------------------------------------
    inline const std::string LogicGate::getNodeType() const {
        return lookupNodeText(mType);
    }
//--------------------------------------------------------------------------------------------------------------------------
    inline void LogicGate::setState(const NodeState new_state) {
        mState = new_state;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    inline void LogicGate::walk(std::ostream& os, int8_t depth) const {
        os << std::setw(3 * depth) << ' ' << getNodeType() << " [" << getName() << "] " /* << typeid( *this).name()*/<< '\n';
    }
//--------------------------------------------------------------------------------------------------------------------------
    inline std::ostream& operator<<(std::ostream& os, const LogicGate& gn) {
        os << gn.getStateText(gn.getState());
        return os;
    }

#endif /* LOGICGATE_H_ */

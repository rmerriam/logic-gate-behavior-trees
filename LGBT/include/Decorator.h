#ifndef Decorator_H_
#define Decorator_H_
//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Jun 5, 2019
//
//======================================================================================================================
template <typename LogicGate::NodeType NT = LogicGate::Decorator>
class Decorator : public LogicGate {
public:
    explicit Decorator(const std::string& name, LogicGatePtr g) :
        LogicGate { NT, name }, mG { std::move(g) } {
//        std::cerr << "Explicit " << name << ' ' << lookupNodeText(NT) << '\n';
    }

    template <typename T, typename = check_type<T>>
    Decorator(T&& g) :
        Decorator { lookupNodeText(NT), g } {
//        std::cerr << "No Name " << lookupNodeText(NT) << '\n';
    }

    template <typename T, typename = check_type<T>>
    Decorator(const std::string& name, T&& g) :
        Decorator { name, make_concrete(g) } {
//        std::cerr << "Named " << name << ' ' << lookupNodeText(NT) << '\n';
    }

    virtual void walk(std::ostream& os, int8_t depth) const override {
        LogicGate::walk(os, depth);
        ++depth;
        mG->walk(os, depth);
        --depth;
    }

protected:
    LogicGatePtr mG;
};

#endif /* Decorator_H_ */

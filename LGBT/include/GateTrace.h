#ifndef Trace_H_
#define Trace_H_
//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 23, 2019
//
//======================================================================================================================
#include <iostream>
#include <iomanip>
#include "LogicGate.h"
//---------------------------------------------------------------------------------------------------------------------
class GateTrace {
public:
    GateTrace(const LogicGate& gn) :
        mNode { gn } {
        ++mDepth;
        trace() << "begin" << std::endl;
    }
    ~GateTrace() {
        trace() << mNode << std::endl;
        --mDepth;
    }
    static void endl() {
        std::cerr << std::endl;
    }
    std::ostream& trace();
    std::ostream& indent();

    static void off() {
        std::cerr.setstate(std::ios_base::badbit);
    }
    static void on() {
        std::cerr.clear();
    }
private:
    static int8_t mDepth;
    const LogicGate& mNode;
};
//---------------------------------------------------------------------------------------------------------------------
inline std::ostream& GateTrace::indent() {
    std::cerr << std::setw(mDepth * 2) << " ";
    return std::cerr;
}
//---------------------------------------------------------------------------------------------------------------------
inline std::ostream& GateTrace::trace() {
    indent() << mNode.getNodeType() << " [" << mNode.getName() << "] ";
    return std::cerr;
}
//---------------------------------------------------------------------------------------------------------------------
struct GateTraceOn {
    GateTraceOn() {
        GateTrace::on();
    }

    GateTraceOn(const auto& text) {
        GateTrace::on();
        std::cerr << "trace on: " << text << '\n';
    }
    ~GateTraceOn() {
        std::cerr << '\n';
        GateTrace::off();
    }
};

#endif /* Trace_H_ */


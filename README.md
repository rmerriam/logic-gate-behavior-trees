# Logic Gate Behavior Trees

How Behavior Trees be represented and implemented as basic logic gate, i.e. AND, OR, NOT. Then how they can be optimized using Boolean Algebra, especially after modifications through training or other automated modification processes, e.g. genetic algorithms.